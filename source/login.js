/**
 * ABCorpID app
 * Login class. First screen shown.
 * Pius Ott - 20180323
 * @flow
 */

 'use strict'
 import React, { Component } from 'react';
 import {
   Alert,
   View,
   Text,
   Image,
   TouchableHighlight,
   TouchableOpacity,
   SafeAreaView,
 } from 'react-native';
import PersoData from './persoData';
import BusyIndicator from './busyIndicator';
import VersionNumber from 'react-native-version-number';

// --------------------------------------------------------------------------------------------------------------------

class Login extends React.Component
{
  // ------------------------------------------------------------------------------------------------------------------

  constructor(props)
  {
    super(props);

    // For this demo, we're not worried about the actual pin typed in, we just want to get the user to press six buttons
    //this.buttonCount = 0;
    this.state = {
       buttonCount : 0,
       loading: false
    }

    this.persoCode = '';
    this.doResetPerso = false;  // if true, we use the PIN buttons to enter a code to receive the perso instead
    this.resetCounter = 0;

    console.log('*** appVersion : ' + VersionNumber.appVersion);
    console.log('*** buildVersion : ' + VersionNumber.buildVersion);
    console.log('*** bundleVersion : ' + VersionNumber.bundleIdentifier);
/*
  // TODO : use a promise from loading PersoData to set this
    if (persoData === null)
    {
      console.log('*** No perso data available yet. Force a reset.');
      this.doResetPerso = true;
    }*/
  }

  // ------------------------------------------------------------------------------------------------------------------

  // ------------------------------------------------------------------------------------------------------------------

  loginButtonHandler(buttonValue)
  {
    if (this.doResetPerso === true)
    {
      this.persoCode = this.persoCode + buttonValue;

      // dummy update to force a screen refresh
      this.setState({ buttonCount: this.state.buttonCount});

      console.log('*** Reset Perso Button Press [' + buttonValue + '] - [' + this.persoCode + ']');
      if (this.persoCode.length >= 6)
      {
        var localPersoCode = this.persoCode;

        this.setState({ loading: true});    // disable this screen and show loading indicator while we're getting data
        console.log('*** Reset perso data with code : ' + localPersoCode);

        PersoData.LoadServerPersoData(localPersoCode)
          .then( () => {
            console.log('*** Finished loading perso data for ' + localPersoCode + ' from server');

            // Use this for testing only. The server query is too quick, let me see what the modal screen looks
            // like for a bit...
            //setTimeout(() => { this.setState({ loading: false}); }, 15000);
            this.setState({ loading: false});
            }
          )
          .catch( () => {
            console.log("*** Error loading perso from server");
            this.setState({ loading: false});

            // I'm sure there's a much more elegant way to do this, I want to close the loading screen but then also
            // show an error alert.
            setTimeout(() => {
              Alert.alert('Perso Loading Error', 'Could not load perso from server for code [' + localPersoCode + ']');
            }, 500);
          });

        this.doResetPerso = false;
        this.persoCode = '';
      }
    }
    else
    {
      this.setState({ buttonCount: this.state.buttonCount + 1});
      console.log('*** Login Button Press [' + buttonValue + '] - Button Count [' + this.state.buttonCount + ']');

      if (this.state.buttonCount >= 5)
      {
        console.log('*** Login successful. Navigating to Page 1');
        //const { navigate } = this.props.navigation;
        this.props.navigation.navigate('Home');

        this.resetCounter = 0;
        this.setState({ buttonCount: 0});
      }
    }
  }


  // ------------------------------------------------------------------------------------------------------------------

  resetPerso()
  {
    this.resetCounter = this.resetCounter + 1;
    //console.log('*** Reset Perso Press  [' + this.resetCounter + ']');

    if (this.resetCounter >= 5)
    {
      console.log('*** Resetting Perso - start code entry');

      this.doResetPerso = true;
      this.resetCounter = 0;

      // reset any part of the pin that has been entered so far. We want a new login after resetting the perso
      this.setState({ buttonCount: 0});
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

  render()
  {
    const styles = require('./style.js').style;
    const colors = require('./style.js').color;

    this.buttonIndex = 0;
    var smallCircle = require('./images/emptyCircleSmall.png')
    var smallCircleFull = require('./images/filledCircleSmall.png')

    function PushIndicator(props)
    {
        const buttonCount = props.buttonCount;
        const buttonIndex = props.buttonIndex;

        if (buttonCount < buttonIndex)
          return <Image style={styles.containerConfigIcon} source={smallCircle} />;

        return <Image style={styles.containerConfigIcon} source={smallCircleFull} />;
    }

    var loginText = <Text style={[styles.titleText, {marginBottom: '15%'}]}>LOGIN</Text>;
    if (this.doResetPerso === true)
    {
      loginText = <Text style={[styles.titleText, {marginBottom: '15%', color:colors.red}]}>
        ENTER PERSO CODE : {this.persoCode}
      </Text>;
    }

    // The login screen contains a 'hidden' function to allow users to set a new perso data set. Clicking
    // on the ABCorp logo six times will change the mode of the login pin buttons to allow a new perso PIN
    // to be entered, which will then download the corresponding perso from the server.
    return (
      <SafeAreaView style={[styles.safeArea, {backgroundColor: colors.white}]}>

        <BusyIndicator
          loading={this.state.loading} />

        <TouchableOpacity style={styles.logoImg}
              activeOpacity = {1}
              onPress={() => this.resetPerso(null)} >
          <Image
              style={{flex:1,
                width: null,
                height: null,
                resizeMode: Image.resizeMode.contain}}
              source={require('./images/ABCorp.png')} />
        </TouchableOpacity>

        <View style={styles.viewLoginDots}>
          <PushIndicator buttonIndex={1} buttonCount = {this.state.buttonCount}/>
          <PushIndicator buttonIndex={2} buttonCount = {this.state.buttonCount}/>
          <PushIndicator buttonIndex={3} buttonCount = {this.state.buttonCount}/>
          <PushIndicator buttonIndex={4} buttonCount = {this.state.buttonCount}/>
          <PushIndicator buttonIndex={5} buttonCount = {this.state.buttonCount}/>
          <PushIndicator buttonIndex={6} buttonCount = {this.state.buttonCount}/>
        </View>

        {loginText}

        <View style={styles.viewLoginButtons}>

          <View style = {styles.menuRow}>
            <TouchableHighlight style={styles.roundButton}
              onPress={() => this.loginButtonHandler('1')} >
              <Text style={styles.roundButtonText}>1</Text>
            </TouchableHighlight>

            <TouchableHighlight style={styles.roundButton}
              onPress={() => this.loginButtonHandler('2')} >
              <Text style={styles.roundButtonText}>2</Text>
            </TouchableHighlight>

            <TouchableHighlight style={styles.roundButton}
              onPress={() => this.loginButtonHandler('3')} >
              <Text style={styles.roundButtonText}>3</Text>
            </TouchableHighlight>
          </View>

          <View style = {styles.menuRow}>
            <TouchableHighlight style={styles.roundButton}
              onPress={() => this.loginButtonHandler('4')} >
              <Text style={styles.roundButtonText}>4</Text>
            </TouchableHighlight>

            <TouchableHighlight style={styles.roundButton}
              onPress={() => this.loginButtonHandler('5')} >
              <Text style={styles.roundButtonText}>5</Text>
            </TouchableHighlight>

            <TouchableHighlight style={styles.roundButton}
              onPress={() => this.loginButtonHandler('6')} >
              <Text style={styles.roundButtonText}>6</Text>
            </TouchableHighlight>
          </View>

          <View style = {styles.menuRow}>
            <TouchableHighlight style={styles.roundButton}
              onPress={() => this.loginButtonHandler('7')} >
              <Text style={styles.roundButtonText}>7</Text>
            </TouchableHighlight>

            <TouchableHighlight style={styles.roundButton}
              onPress={() => this.loginButtonHandler('8')} >
              <Text style={styles.roundButtonText}>8</Text>
            </TouchableHighlight>

            <TouchableHighlight style={styles.roundButton}
              onPress={() => this.loginButtonHandler('9')} >
              <Text style={styles.roundButtonText}>9</Text>
            </TouchableHighlight>
          </View>

          <View style = {styles.menuRow}>
            <TouchableHighlight style={styles.roundButton}
              onPress={() => this.loginButtonHandler('0')} >
              <Text style={styles.roundButtonText}>0</Text>
            </TouchableHighlight>

          </View>

          <Text style={[styles.infoText, {textAlign: 'center', marginTop: 10}]}>
            v{VersionNumber.appVersion} {VersionNumber.buildVersion}
          </Text>

        </View>
      </SafeAreaView>
    );

  }

  // ------------------------------------------------------------------------------------------------------------------

}

Login.navigationOptions = {
  header: null
};

export default Login;
