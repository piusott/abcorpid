/**
 * ABCorpID app
 * PersoData - Holds the app's personalisation data
 * Pius Ott - 20180403
 * @flow
 */


'use strict'
import React, { Component } from 'react';
import {
  AsyncStorage,
} from 'react-native';
import RNFetchBlob from 'react-native-fetch-blob';
import DeviceInfo from 'react-native-device-info';

// --------------------------------------------------------------------------------------------------------------------

const storageKeyData = '@ABCorpID:persoData';    // storage location of our perso data
const storageKeyPort = '@ABCorpID:portrait';     // Portrait image in base64
const storageKeySig = '@ABCorpID:signature';     // Signature image in base64
const storageKeyCode = '@ABCorpID:persoKey';     // perso key that lets us find the correct perso data on the server

global.persoData = null;
global.portraitImg = null;
global.signatureImg = null;

export default class PersoData extends Component {

  static keyCode = '';

  constructor(props)
  {
    super(props);

    this.LoadPersoCode()
      .then( () => { this.LoadLocalPersoData(PersoData.keyCode); })
      .catch(error => {
        console.log('*** Error on loading perso code : ' + error);

        PersoData.CreateDemoPersoData();  // create the sample perso and store it locally
      });
  }

  // ------------------------------------------------------------------------------------------------------------------

  // Create a demo set of perso data. This is the default we use until we are requested to download fresh perso data
  // from the server.
  static CreateDemoPersoData()
  {
    const defaultKeyCode = '852369'; // Pius default perso

    try
    {
      console.log('*** No perso data available. Creating a demo set.');
      persoData = require('../SamplePersoProfiles/' + defaultKeyCode + '/perso.json');

      // Load the base64 image data since we're using base64 throughout the app already
      portraitImg = require('../SamplePersoProfiles/' + defaultKeyCode + '/portrait.json').portrait;
      //console.log('*** portData : ' + portraitImg);

      signatureImg = require('../SamplePersoProfiles/' + defaultKeyCode + '/signature.json').signature;
      //console.log('*** SigData : ' + signatureImg);

      // Storing perso, images and keycode in local cache
      AsyncStorage.setItem(storageKeyData, JSON.stringify(persoData));
      AsyncStorage.setItem(storageKeyCode, defaultKeyCode);
      AsyncStorage.setItem(storageKeyPort, portraitImg);
      AsyncStorage.setItem(storageKeySig, signatureImg);
    }
    catch (e)
    {
      console.log("*** Exception in CreateDemoPersoData - " + e);
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

  // Load new perso from server and store in local cache
  static LoadServerPersoData(persoCode)
  {
    return new Promise(function(resolve, reject)
    {
      if ((persoCode === null) || (persoCode.length < 6))
      {
        console.log('*** Invalid persoCode [' + persoCode + ']. Using default instead');
        persoCode = '852369';  // default perso ID
      }

      var serverURL = 'http://abidtest.abnote.com.au/ABCorpID/profiles/' + persoCode;
      if (DeviceInfo.isEmulator())
      {
        // If we're running in the simulator, ABCorp firewall restrictions don't allow us easy access to the perso server,
        // so use a local web server address instead. Note that these are different servers, so we need to make sure
        // the perso details are synched.
        console.log('*** Running in simulator');
        serverURL = 'http://172.31.64.64/abcorpid/profiles/' + persoCode;
      }

      console.log('*** Loading perso from ' + serverURL);

      fetch(serverURL + '/perso.json', {
        headers: {
          'Cache-Control': 'no-cache'
        }})
        .then((response) => response.json())
        .then((responseJson) =>
        {
          console.log("*** Fetched JSON data successfully - updating local store");

          try
          {
            let textData = JSON.stringify(responseJson);
            console.log("*** Fetched JSON : " + textData);

            // Save the fetched perso data locally, overwriting any previous perso data
            PersoData.LoadImages(serverURL);  // also saves the images in local store
            AsyncStorage.setItem(storageKeyData, textData);
            AsyncStorage.setItem(storageKeyCode, persoCode);  // store the persoCode so we can use that on next startup
            persoData = responseJson;
          }
          catch (e)
          {
            console.log("*** Could not save persoData to local : " + e);
          }

          resolve();
        })
        .catch(error => {
          console.log('*** Error on loading perso code. Not overwritting local store perso. : ' + error);
          // We are now creating a perso on startup, if none is found or can be loaded. So we can assume that when this
          // function is called we will always have a local perso already. So in an exception do not clear our local
          // perso. I don't want people to lose their perso just because the server can't be reached.
          //PersoData.CreateDemoPersoData();  // create a sample perso instead

          reject();
        });
    });
  }

  // ------------------------------------------------------------------------------------------------------------------

  static LoadImages(serverURL)
  {
    function getImageBase64(mimetype_attachment, imgURL)
    {
      //console.log("*** Using server URL : " + fullServerURL);

      return new Promise((RESOLVE, REJECT) =>
      {
        // Fetch attachment
        RNFetchBlob.fetch('GET', imgURL).then((response) =>
        {
          let base64Str = response.data;
          //console.log("*** received : " + base64Str);
          var imageBase64 = 'data:' + mimetype_attachment + ';base64,' + base64Str;
          //console.log("*** base64Str is type : " + typeof(base64Str));

          RESOLVE(imageBase64)  // return base64 image
        })
      }).catch((error) => {
         // error handling
        console.log("*** Error fetching image : ", error)
      });
    }

    getImageBase64('image/jpeg', serverURL + '/portrait.jpg').then((base64Response) =>
    {
      if (base64Response.lastIndexOf('404 Not Found') < 0)
      {
        console.log("*** We have an image. Also store portrait in AsyncStorage");
        //console.log("*** " + base64Response);
        try
        {
          portraitImg = base64Response;
          AsyncStorage.setItem(storageKeyPort, base64Response);
        }
        catch (e)
        {
          console.error("*** Could not store portrait image : " + e);
        }
      }
    });

    getImageBase64('image/jpeg', serverURL + '/signature.png').then((base64Response) =>
    {
      if (base64Response.lastIndexOf('404 Not Found') < 0)
      {
        console.log("*** We have an image. Also store signature in AsyncStorage");
        //console.log("*** " + base64Response);
        try
        {
          signatureImg = base64Response;
          AsyncStorage.setItem(storageKeySig, base64Response);
        }
        catch (e)
        {
          console.error("*** Could not store signature image : " + e);
        }
      }
    });
  }

  // ------------------------------------------------------------------------------------------------------------------

  // Load the perso data from the local cache
  LoadLocalPersoData()
  {
    function getImage(storageKey)
    {
      return new Promise((RESOLVE, REJECT) =>
      {
        AsyncStorage.getItem(storageKey, (err, result) =>
        {
          let img64 = result;
          if (img64 !== null)
          {
            console.log('*** Loaded image [' + storageKey + '] from local');
            if (storageKey === storageKeySig)   ///// TODO : I'm sure that can be done better!
              signatureImg = img64;
            else
              portraitImg = img64;
          }
        })
      }).catch((error) => {
         // error handling
        console.log('*** Error fetching image from [' + storageKey + '] : ' + error)
      });
    }

    try
    {
      AsyncStorage.getItem(storageKeyData, (err, result) =>
      {
        let tmpPersoData = JSON.parse(result)
        if (tmpPersoData != null)
        {
          console.log("*** loaded persoData JSON : " + result);
          persoData = tmpPersoData;
        }
        else
        {
          console.log("*** No perso data found, create default");
          PersoData.CreateDemoPersoData();
        }
      }).then(getImage(storageKeySig)).then(getImage(storageKeyPort));
    }
    catch (e)
    {
      console.log("*** Could not load persoData from local : " + e);
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

  // Load the six digit perso code from the cache. If we have a perso code, it's assumed we also have a corresponding
  // personalisation set stored in the cache
  LoadPersoCode()
  {
    return new Promise(function(resolve, reject)
    {
      // try and load the perso code from our local storage. If we can't load this, it likely means there is no perso yet
      try
      {
        AsyncStorage.getItem(storageKeyCode, (err, result) =>
        {
          //let keyCode = JSON.parse(result);
          console.log("*** load keyCode from AsyncStorage result : " + result);

          if ((result === null) || (result === undefined))
          {
            console.log("*** No keyCode found.");
            PersoData.keyCode = '';

            // Reject and let the calling function handle this (ideally by creating a sample perso set)
            reject();
          }
          else
          {
            PersoData.keyCode = result;
            console.log('*** Resolving keyCode loaded promise with keyCode [' + PersoData.keyCode + '].');
            resolve();
          }
        });
      }
      catch (e)
      {
        console.log("*** Error loading keyCode : " + e);
        reject(e);
      }
    });
  }

  // ------------------------------------------------------------------------------------------------------------------

}
