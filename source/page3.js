'use strict';

import React, { Component } from 'react';
import {
  Dimensions,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';

import {StackNavigator} from 'react-navigation';
import QRCode from 'react-native-qrcode';


class Page3 extends Component
{
  constructor(props)
  {
    super(props);

    // Calculate our display positions. We need to use absolute positions based on the screen dimensions because we
    // have to make sure that we can overpaint the perso data at the correct size in the correct positions on any screen
    // sizes, which wouldn't work with a flex layout.
    this.cardWidth = windowWidth / 100 * 92; // 92% of screen width, we're using 4% margins to centre this
    this.cardHeight = this.cardWidth * 1.552;     // height to width ratio of the card image
    //    this.cardTop = (windowHeight / 2) - (this.cardHeight / 2);         // position image in middle of the screen
    this.cardTop = (windowHeight / 2.2) - (this.cardHeight / 2);         // position slightly above the middle of the screen to compensate for page indicator

    //console.log('*** Card Dimensions w [' + this.cardWidth + '] - h [' + this.cardHeight + '] - top [ ' + this.cardTop + ']');

    /* tmp hack to make the display work on iPhone SE screens which have a somewhat odd ratio.
      This needs to be fixed properly to work on all screen sizes, but for the moment I just need this to work quickly
      for a demo.
    */
    let seFactor = 0;
    let windowRatio = windowHeight / windowWidth;
    console.log('*** Window Width and Height [' + windowWidth + '] - [' + windowHeight + '] - ratio [' + windowRatio + ']');
    if (windowRatio == 1.775)  // iPhone SE screen height to width ratio
    {
      seFactor = 0.8;
      console.log('*** adjusting SE factor for iPhone SE screens');
    }
    // Calculate font sizes based on our card width
    this.font12 = this.cardWidth * 0.0348 - seFactor;
    this.font14 = this.cardWidth * 0.0406 - seFactor;
    this.font16 = this.cardWidth * 0.0464 - seFactor;
    this.font19 = this.cardWidth * 0.0551 - seFactor;
    this.font26 = this.cardWidth * 0.0753 - seFactor;

    this.persoLeft = this.cardHeight * 0.045 + (seFactor * 20);
    this.persoCol1 = this.cardHeight * 0.260 + (seFactor * 20);
    this.persoCol2 = this.cardHeight * 0.360 + (seFactor * 20);
    this.persoCol3 = this.cardHeight * 0.320 + (seFactor * 20);   // {Sex}
    this.persoCol4 = this.cardHeight * 0.370 + (seFactor * 20);   // EYES
    this.persoCol5 = this.cardHeight * 0.445 + (seFactor * 20);   // {Eyes}
    this.persoCol6 = this.cardHeight * 0.530 + (seFactor * 20);   // Height
    this.persoCol7 = this.cardHeight * 0.630 + (seFactor * 20);   // {height}
    this.persoCol8 = this.cardHeight * 0.480 + (seFactor * 20);   // {CVV}
    this.persoCol9 = this.cardHeight * 0.545 + (seFactor * 20);   // GOOD THRU
    this.persoCol10 = this.cardHeight * 0.630 + (seFactor * 20);  // {ExpiryDate}

    this.persoRow1 = this.cardWidth * 0.260 + (seFactor * 20);    // GOOD
    this.persoRow2 = this.cardWidth * 0.300 + (seFactor * 20);    // THRU
    this.persoRow3 = this.cardWidth * 0.380 + (seFactor * 20);    // NAME
    this.persoRow4 = this.cardWidth * 0.460 + (seFactor * 20);    // TITLE
    this.persoRow5 = this.cardWidth * 0.540 + (seFactor * 20);    // Office
    this.persoRow6 = this.cardWidth * 0.620 + (seFactor * 20);    // SEX
    this.persoRow7 = this.cardWidth * 0.385 + (seFactor * 20);    // QR Code
    this.persoRow8 = this.cardWidth * 0.706 + (seFactor * 20);    // Signature
    this.persoRow9 = this.cardWidth * 0.700 + (seFactor * 20);    // CVV

    this.qrSize = this.cardHeight * 0.179 - (seFactor * 6);
    this.sigHeight = this.cardWidth * 0.146 - (seFactor * 20);
    this.sigWidth = this.cardHeight * 0.374 - (seFactor * 20);
  }

  // ------------------------------------------------------------------------------------------------------------------

  render() {
    const { navigate, goBack } = this.props.navigation;
    const styles = require('./style.js').style;
    const colors = require('./style.js').color;

    // Not sure why, but the 'transform' seems to position the view oddly.
    // TODO : Need to test this on different screen sizes to see if it's a constant or something that needs to be calculated
    const rotateOffset = 95;

    // Note : I'm not using a SaveAreaView here because I want to have the card always right in the horizontal centre
    // of the screen. Because we're not painting anything right at the top or bottom of the screen, we should be safe
    // either way.
    return (
      <View style={styles.safeArea}>
        <TouchableOpacity onPress={() => navigate('Home')}>
          <ImageBackground
            style={{top:this.cardTop, width:this.cardWidth, height:this.cardHeight, marginLeft:'4%'}}
            source={require('./images/Blank_Back.png')}
            resizeMode="contain" >

            <View style={{
              transform: [{ rotate: '270deg'}],
              width: this.cardHeight,
              height: this.cardWidth,
              top: rotateOffset,
              left: -rotateOffset,
              }}>

              <Text style={[styles.cardReversePINText, {fontSize:this.font26, top:this.persoRow1, left:this.persoLeft}]}>
                {persoData.PIN}
              </Text>
              <Text style={[styles.cardReverseBoldText, {fontSize:this.font12, top:this.persoRow1 + 1, left:this.persoCol9}]}>
                GOOD
              </Text>
              <Text style={[styles.cardReverseBoldText, {fontSize:this.font12, top:this.persoRow2, left:this.persoCol9}]}>
                THRU
              </Text>
              <Text style={[styles.cardReversePINText, {fontSize:this.font26, top:this.persoRow1, left:this.persoCol10}]}>
                {persoData.ExpiryDate}
              </Text>

              <Text style={[styles.cardReverseLabelText, {fontSize:this.font14, top:this.persoRow3 + 4, left:this.persoCol1}]}>
                NAME
              </Text>
              <Text style={[styles.cardReverseLabelText, {fontSize:this.font14, top:this.persoRow4 + 4, left:this.persoCol1}]}>
                TITLE
              </Text>
              <Text style={[styles.cardReverseLabelText, {fontSize:this.font14, top:this.persoRow5 + 4, left:this.persoCol1}]}>
                OFFICE
              </Text>
              <Text style={[styles.cardReverseLabelText, {fontSize:this.font14, top:this.persoRow6 + 4, left:this.persoCol1}]}>
                SEX
              </Text>
              <Text style={[styles.cardReverseLabelText, {fontSize:this.font14, top:this.persoRow6 + 4, left:this.persoCol4}]}>
                EYES
              </Text>
              <Text style={[styles.cardReverseLabelText, {fontSize:this.font14, top:this.persoRow6 + 4, left:this.persoCol6}]}>
                HEIGHT
              </Text>

              <Text style={[styles.cardReverseDataText, {fontSize:this.font19, top:this.persoRow3, left:this.persoCol2}]}>
                {persoData.GivenNames} {persoData.Surname}
              </Text>
              <Text style={[styles.cardReverseDataText, {fontSize:this.font19, top:this.persoRow4, left:this.persoCol2}]}>
                {persoData.Title}
              </Text>
              <Text style={[styles.cardReverseDataText, {fontSize:this.font19, top:this.persoRow5, left:this.persoCol2}]}>
                {persoData.Office}
              </Text>
              <Text style={[styles.cardReverseDataText, {fontSize:this.font19, top:this.persoRow6, left:this.persoCol3}]}>
                {persoData.Sex}
              </Text>
              <Text style={[styles.cardReverseDataText, {fontSize:this.font19, top:this.persoRow6, left:this.persoCol5}]}>
                {persoData.Eyes}
              </Text>
              <Text style={[styles.cardReverseDataText, {fontSize:this.font19, top:this.persoRow6, left:this.persoCol7}]}>
                {persoData.Height}
              </Text>

              <Text style={[styles.cardReverseCVVText, {fontSize:this.font16, top:this.persoRow9, left:this.persoCol8}]}>
                {persoData.CVV}
              </Text>

              <View style={{top: this.persoRow7, left: this.persoLeft + 9}}>
                <QRCode
                  value={
                    persoData.GivenNames + ' ' +
                    persoData.Surname + '\n' +
                    persoData.Title + '\n' +
                    persoData.Company + '\n' +
                    persoData.City + '\n' +
                    persoData.Country + '\n' +
                    persoData.URL}
                  size={this.qrSize}
                  bgColor='black'
                  fgColor='white'/>

              </View>

              <Image
                style={[styles.portImg, {height:this.sigHeight, width:this.sigWidth, left:this.persoLeft + 5, top:this.persoRow8}]}
                source={{uri: signatureImg}} />

            </View>

          </ImageBackground>
        </TouchableOpacity>

        <View style={styles.containerIndImg}>
          <Image style={styles.pageIndImg} source={require('./images/page3.png')} />
        </View>

      </View>
    );
  }
};

// --------------------------------------------------------------------------------------------------------------------

Page3.navigationOptions = {
  header: null
};

export default Page3;
