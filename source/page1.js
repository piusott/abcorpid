/**
 * ABCorpID app
 * Page 1 - main screen with ID and QR code
 * Pius Ott - 20180327
 * @flow
 */

 'use strict'
 import React, { Component } from 'react';
 import {
   Alert,
   AppRegistry,
   Button,
   Dimensions,
   StyleSheet,
   View,
   Text,
   Image,
   ImageBackground,
   TouchableOpacity,
   SafeAreaView,
 } from 'react-native';
import {StackNavigator} from 'react-navigation';
import QRCode from 'react-native-qrcode';

// --------------------------------------------------------------------------------------------------------------------


class Page1 extends React.Component
{

  constructor(props)
  {
    super(props);

    // To be able to accurately position the variable perso data on the card, we need to know how large the displayed
    // card is and where it's positioned. This allows us to show a like card regardless of the phone's screen size
    // The measuring we do via onLayout is somewhat slow on Android devices. Try the simpler approach via window size
    // The hardcoded values were simply trial and error.
    this.cardWidth = windowWidth / 100 * 92; // 92% of screen width, we're using 4% margins to centre this
    this.cardHeight = this.cardWidth / 1.552;     // height to width ratio of the card image
    //this.cardTop = (windowHeight / 2) - (this.cardHeight / 2);         // position image in middle of the screen
    this.cardSideMargin = (windowWidth - this.cardWidth) / 2;

    this.cardFontSize = this.cardWidth * 0.058;

    this.persoLeft = this.cardWidth * 0.055;
    this.persoRow1 = this.cardHeight * 0.595;    // Name
    //this.persoRowSpacer = this.cardHeight * 0.007;    // Line 2 and 3
    this.persoRowSpacer = 0;
    this.photoWidth = this.cardWidth * 0.320;
    this.photoHeight = this.photoWidth * 1.157;
    this.photoLeft = this.cardWidth * 0.619;
    this.photoTop = this.cardHeight * 0.328;

    this.qrSize = this.cardWidth / 2;

    console.log('*** Window Size : [' + windowWidth + ' x ' + windowHeight + ']');
    console.log('*** Card Size : [' + this.cardWidth + ' x ' + this.cardHeight + ']');
  }

  // ------------------------------------------------------------------------------------------------------------------
/*
  onLayout = (e) => {
    this.setState({
        cardWidth: e.nativeEvent.layout.width,
        cardHeight: e.nativeEvent.layout.height,
        cardX: e.nativeEvent.layout.x,
        cardY: e.nativeEvent.layout.y
      })
     console.log('*** onLayout w [' + this.state.cardWidth + '] - h ['
       + this.state.cardHeight + '] - x [ ' + this.state.cardX + '] - y [' + this.state.cardY + ']');
  }
*/
  // ------------------------------------------------------------------------------------------------------------------

  render()
  {
    const { navigate, goBack } = this.props.navigation;
    const styles = require('./style.js').style;
    const colors = require('./style.js').color;

    return (
      <SafeAreaView style={styles.safeArea}>

          <View style={{height: '8%'}}>
            <Text
              style={[styles.infoText, {marginLeft: '4%'}]}
              onPress={() => navigate('Login')}>
              Log Out
            </Text>
          </View>
          <View style={{height:'35%'}}>
            <TouchableOpacity onPress={() => navigate('PhotoPage')}>
              <ImageBackground
                style={[styles.cardFrontImg, {width:this.cardWidth, height:this.cardHeight, marginLeft:this.cardSideMargin}]}
                source={require('./images/Blank_Front.png')}
                resizeMode="contain" >

                <Image
                  style={[styles.portImg, {height:this.photoHeight, width:this.photoWidth, left:this.photoLeft, top:this.photoTop}]}
                  source={{uri: portraitImg}} />

                <Text style={[styles.cardText, {marginTop: this.persoRow1, marginLeft: this.persoLeft, fontSize: this.cardFontSize}]}>
                  {persoData.GivenNames} {persoData.Surname}
                </Text>
                <Text style={[styles.cardText, {marginTop: this.persoRowSpacer, marginLeft: this.persoLeft, fontSize: this.cardFontSize}]}>
                  {persoData.Title}
                </Text>
                <Text style={[styles.cardText, {marginTop: this.persoRowSpacer, marginLeft: this.persoLeft, fontSize: this.cardFontSize}]}>
                  {persoData.Office}
                </Text>

              </ImageBackground>
            </TouchableOpacity>
          </View>

          <View style={{height: '5%'}}>
          </View>

          <TouchableOpacity onPress={() => navigate('DetailsPage')}>
            <View style={{backgroundColor: 'white',
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: this.qrSize / 2,
              marginRight: this.qrSize / 2,
              height: this.qrSize + (this.qrSize / 5)}}>

              <QRCode
                value={
                  persoData.GivenNames + ' ' +
                  persoData.Surname + '\n' +
                  persoData.Title + '\n' +
                  persoData.Company + '\n' +
                  persoData.City + '\n' +
                  persoData.Country + '\n' +
                  persoData.URL}
                size={this.qrSize}
                bgColor='black'
                fgColor='white'/>
            </View>
          </TouchableOpacity>

          <View style={styles.containerIndImg}>
            <Image style={styles.pageIndImg} source={require('./images/page1.png')} />
          </View>
      </SafeAreaView>
    );
  }

  // ------------------------------------------------------------------------------------------------------------------
}

// --------------------------------------------------------------------------------------------------------------------
Page1.navigationOptions = {
  header: null
};

export default Page1;
