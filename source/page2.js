'use strict';

import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  ImageBackground,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {StackNavigator} from 'react-navigation';

class Page2 extends Component
{
  constructor(props)
  {
    super(props);

    this.portraitHeight = windowHeight / 100 * 60; // 60% of screen height
  }

  // ------------------------------------------------------------------------------------------------------------------
/*
<Image
  style={{width:'100%', resizeMode: Image.resizeMode.contain}}
  source={{uri: portraitImg}} />
*/
  render() {
    const { navigate, goBack } = this.props.navigation;
    const styles = require('./style.js').style;
    const colors = require('./style.js').color;

    return (
      <SafeAreaView style={styles.safeArea}>
        <View style={{marginTop:'10%', marginLeft:'10%', marginRight:'10%'}}>
          <TouchableOpacity onPress={() => navigate('CardBackPage')}>
            <Image
              style={{resizeMode: Image.resizeMode.contain, height:this.portraitHeight, width:'100%'}}
              source={{uri: portraitImg}} />
          </TouchableOpacity>
        </View>

        <View style={styles.containerIndImg}>
          <Image style={styles.pageIndImg} source={require('./images/page2.png')} />
        </View>

      </SafeAreaView>
    );
  }
};

// --------------------------------------------------------------------------------------------------------------------

Page2.navigationOptions = {
  header: null
};

export default Page2;
