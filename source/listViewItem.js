'use strict';

import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';

class ListViewItem extends Component
{
  constructor(props)
  {
    super(props);
  }

  render()
  {
    const styles = require('./style.js').style;
    const colors = require('./style.js').color;

    return (
      <View style={{flexDirection:'row', margin:4, paddingTop:4, paddingBottom:4, backgroundColor:colors.white}}>
        <Text
          style={[styles.label, {marginLeft: '4%'}]}>
          {this.props.Label}
        </Text>

        <Text
          style={[styles.infoText, {marginLeft: 0}]}>
          {this.props.Data}
        </Text>

      </View>
    )
  }
}

export default ListViewItem
