/**
 * ABCorpID app
 * Stack Navigator, started after logging in
 * Pius Ott - 20180326
 * @flow
 */

'use strict'
import React, { Component } from 'react';
import {
   AppRegistry,
   Dimensions,
   Text
 } from 'react-native';

import {StackNavigator} from 'react-navigation';
import Login from './login.js';
import Page1 from './page1.js';
import Page2 from './page2.js';
import Page3 from './page3.js';
import Page4 from './page4.js';
import PersoData from './persoData';

// --------------------------------------------------------------------------------------------------------------------

Text.defaultProps.allowFontScaling=false;   // Disable font scaling. This messes with our measured screen layout

try
{
  global.windowWidth = Dimensions.get('window').width;
  global.windowHeight = Dimensions.get('window').height;
}
catch (e)
{
  // Not sure why, but occasionally I get exceptions when measuring the screen dimensions on startup. If that happens
  // just default to an iPhone 6 screen size. This may end up looking a bit odd on other sizes, but should still be
  // presentable
  console.log('*** Exception on measuring the screen size : ' + e);
  global.windowWidth = 375;
  global.windowHeight = 667;
}

// --------------------------------------------------------------------------------------------------------------------

// Loads / initialises the personalisation data
new PersoData();

// --------------------------------------------------------------------------------------------------------------------

const Navigator = StackNavigator(
  {
    ////// temp, making this the startup screen while I'm designing it, so I don't have to keep pushing buttons...
    //DetailsPage: { screen: Page4 },

    Login: { screen: Login },
    Home: { screen: Page1 },
    PhotoPage: { screen: Page2 },
    CardBackPage: { screen: Page3 },
    DetailsPage: { screen: Page4 },
  },
  {
    //headerMode: 'none',
    headerMode: 'screen',
    //headerMode: 'float',
  },
);

AppRegistry.registerComponent('ABCorpID', () => Navigator);
