/**
 * Global style sheet for all pages in the application
 **/

'use strict';

import {
  Button,
  Dimensions,
  StyleSheet,
  View,
  Text,
  Image,
  Platform,
} from 'react-native';

const Colors =
{
  bgColor: '#1A304B',
  bgEmphasis: '#16273D',
  headerColor: '#1F3959',
  fgTextLight: '#C1E5FF',
  fgTextDark: '#3A3B3D',
  fgTextLightGray: '#EDEDED',
  fgTextMedGray: '#A2A2A2',
  lightBlue: '#68A0CF',
  darkBlue: '#07213D',
  medGray: '#8B9296',
  white: '#FFFFFF',
  black: '#000000',
  red: '#FF0000',
  titleColor: '#606060',
  failTitleColor: '#FF4040',
  bgBlue: '#073054',
  bgGreen: '#3A8734',
  //bgPass: '#F3FFF2',  // green tint
  bgPass: '#D8F7D4',
  bgFail: '#FFE5E5',   // red tint
};

module.exports = {
color: Colors,
style: StyleSheet.create({
  logoImg: {
    flex: 1,
    width: '40%',
    marginRight: '30%',
    marginLeft: '30%',
    //marginBottom:'2%',
    //resizeMode: Image.resizeMode.contain,
  },
  containerConfigIcon: {
    width: '6.5%',
    marginLeft: '5%',
    marginRight: '5%',
    resizeMode: Image.resizeMode.contain,
  },
  safeArea: {
    //flex: 4,
    height: '100%',
    backgroundColor: Colors.bgBlue,
  },
  titleText:{
    color: Colors.fgTextDark,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '800',
  },
  viewLoginDots:{
    //flex: 1,
    marginRight: '25%',
    marginLeft: '25%',
    //height: '8%',
    flexDirection: 'row',
    //backgroundColor: Colors.bgColor,   // TESTING
  },
  viewLoginButtons:{
    //flex: 1,
    marginRight: '5%',
    marginLeft: '5%',
    //flexDirection: 'row',
    //backgroundColor: Colors.lightBlue,  // TESTING
  },
  roundButton: {
    //marginTop:'10%',
    //margin: '10%',
    marginLeft:'9%',
    marginRight:'9%',
    paddingTop:7,
    borderRadius:40,
    width: 70,
    height: 70,
    borderWidth: 2,
    borderColor: Colors.bgColor,
  },
  roundButtonText: {
    color: Colors.fgTextDark,
    textAlign: 'center',
    fontSize: 45,
    fontWeight: '200',
    fontFamily: 'Helvetica Neue',
  },
  menuRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    height:'18%',
    //backgroundColor: Colors.bgGreen,  // TESTING
  },
  cardFrontImg: {
    width: undefined,
    height: '100%',
  },
  pageIndImg: {
    //flex: 2,
    width: 60,
    height: 12,
  },
  containerIndImg: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: '3%',
  },
  cardText: {
    color: Colors.black,
    textAlign: 'left',
    //fontSize: 20,
    fontWeight: '500',
    fontFamily: 'Helvetica Neue',
    //marginLeft: '7%',
  },
  cardReversePINText: {
    //textAlign: 'left',
    color: Colors.white,
    position: 'absolute',
    fontWeight: '800',
    fontFamily: 'Helvetica Neue',
    //transform: [{ rotate: '270deg'}, {scaleY: 1.05}],
    transform: [{scaleY: 1.05}],
  },
  cardReverseBoldText: {
    //textAlign: 'left',
    color: Colors.white,
    position: 'absolute',
    fontWeight: '800',
    fontFamily: 'Helvetica Neue',
    //transform: [{ rotate: '270deg'}],
  },
  cardReverseLabelText: {
    //textAlign: 'center',
    color: Colors.white,
    position: 'absolute',
    fontWeight: '300',
    fontFamily: 'Helvetica Neue',
    //transform: [{ rotate: '270deg'}, {scaleX: 0.81}],
    transform: [{scaleX: 0.81}],
  },
  cardReverseDataText: {
    //textAlign: 'left',
    color: Colors.white,
    position: 'absolute',
    fontWeight: '400',
    fontFamily: 'Helvetica Neue',
    //transform: [{ rotate: '270deg'}, {scaleY: 1.05}],
  },
  cardReverseCVVText: {
    //textAlign: 'left',
    color: Colors.black,
    position: 'absolute',
    fontWeight: '400',
    fontFamily: 'HelveticaNeue-Italic',
    //transform: [{ rotate: '270deg'}, {scaleY: 1.05}],
  },
  label: {
    fontSize: 12.5,
    textAlign: 'left',
    color: Colors.darkBlue,
    marginBottom: 4,
    marginTop: 4,
    marginRight: 10,
  },
  infoText: {
    fontSize: 12.5,
    textAlign: 'left',
    marginBottom: 4,
    marginTop: 4,
    color: Colors.lightBlue,
    fontWeight: 'normal',
  },
  pageHeaderText: {
    fontSize: 12.5,
    textAlign: 'center',
    color: Colors.white,
    marginBottom: 4,
    marginTop: 4,
    fontWeight: '700',
  },
  portImg: {
    resizeMode: Image.resizeMode.contain,
    margin:0,
    padding:0,
    position: 'absolute',
  },

}),
}
