/**
 * ABCorpID app
 * Busy indicator that we can show while loading perso data from the server.
 * This is covers the whole screen but is also transparent so that we can essentially "disable" the screen behind
 * it and only show the activity indicator in the screen centre.
 * Pius Ott - 20180423
 * @flow
 */

'use strict'
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text,
  ActivityIndicator
} from 'react-native';

const BusyIndicator = props => {
  const {
    loading,
    ...attributes
  } = props;

const styles = require('./style.js').style;
//const colors = require('./style.js').color;

return (
    <Modal
      transparent={true}
      animationType={'none'}
      onRequestClose={() => {}}
      visible={loading}>
      <View style={localStyles.modalBackground}>
        <View style={localStyles.activityIndicatorWrapper}>
          <Text style={[styles.titleText, {marginLeft:'5%', marginRight:'5%'}]}>Loading Personalisation</Text>
          <ActivityIndicator animating={loading} />
        </View>
      </View>
    </Modal>
  )
}

const localStyles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 150,
    width: 200,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
});

export default BusyIndicator;
