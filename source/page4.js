'use strict';

import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {StackNavigator} from 'react-navigation';
import ListViewItem from './listViewItem.js';

class Page4 extends Component
{
  constructor(props)
  {
    super(props);
    this.portraitHeight = windowHeight / 100 * 22; // 22% of screen height
  }

  // ------------------------------------------------------------------------------------------------------------------

  render() {
    const { navigate, goBack } = this.props.navigation;
    const styles = require('./style.js').style;
    const colors = require('./style.js').color;

    return (
      <SafeAreaView style={styles.safeArea}>
        <TouchableOpacity onPress={() => navigate('Home')}>

          <Text
            style={styles.pageHeaderText}>
            ID Badge Details
          </Text>

          <Image
            style={{height:this.portraitHeight, resizeMode: Image.resizeMode.contain}}
            source={{uri:portraitImg}} />

        </TouchableOpacity>

        <ScrollView style={{marginBottom:'10%'}}>
          <View style={{marginTop:'2%', marginLeft:'15%', marginRight:'15%', marginBottom:'2%', backgroundColor:colors.darkBlue }}>
            <ListViewItem Label='Name:' Data={persoData.GivenNames} />
            <ListViewItem Label='Last Name:' Data={persoData.Surname} />
            <ListViewItem Label='Title:' Data={persoData.Title} />
            <ListViewItem Label='Group ID:' Data={persoData.GroupID} />
            <ListViewItem Label='Address:' Data={persoData.Address} />
            <ListViewItem Label='City:' Data={persoData.City} />
            <ListViewItem Label='State:' Data={persoData.State} />
            <ListViewItem Label='Zip Code:' Data={persoData.ZipCode} />
            <ListViewItem Label='Office Number:' Data={persoData.OfficePhone} />
            <ListViewItem Label='Mobile Number:' Data={persoData.MobilePhone} />
            <ListViewItem Label='Challenge Word:' Data={persoData.ChallengeWord} />
          </View>
        </ScrollView>

        <View style={styles.containerIndImg}>
          <Image style={styles.pageIndImg} source={require('./images/page4.png')} />
        </View>

      </SafeAreaView>
    );

  }
};

// --------------------------------------------------------------------------------------------------------------------

Page4.navigationOptions = {
  header: null
};

export default Page4;
